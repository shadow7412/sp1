sp1
===

Shadow Prompt (The name is a pun on PS1 - the prompt variable)


Installation
============

    cd ~
    git clone git@bitbucket.org:shadow7412/sp1.git .sp1
    echo source ~/.sp1/activate >> ~/.bashrc
    source ~/.bashrc
    cd -

Or you can get a script to do most of the above for you;

    curl https://bitbucket.org/shadow7412/sp1/raw/master/install | bash
    source ~/.bashrc

Features
========

    * Minimal
    * Shows how many seconds the previous command ran for (unless it ran for under a second)
    * Changes to red if the previous command failed, or had a non zero return code.